<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\PertemuanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/auth', [AuthController::class, 'auth'] );

Route::middleware(['sso'])->group(function () {

    Route::get('/', [DashboardController::class, 'index']);
    Route::get('/logout', [AuthController::class, 'logout'] );
    Route::get('/kelas/{id:uuid}', [KelasController::class, 'get'])->name('kelas');
    Route::get('/kelas/{id_kelas:uuid}/tambah-pertemuan', [PertemuanController::class, 'tambah'])->name('tambah-pertemuan');
    Route::post('/kelas/{id_kelas:uuid}/tambah-pertemuan', [PertemuanController::class, 'tambahAction']);
    Route::get('/pertemuan/{id:uuid}', [PertemuanController::class, 'get'])->name('pertemuan');
    
});