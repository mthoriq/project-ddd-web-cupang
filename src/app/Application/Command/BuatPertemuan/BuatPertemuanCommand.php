<?php

namespace App\Application\Command\BuatPertemuan;

use App\Core\JadwalPertemuan;
use App\Core\KelasId;
use App\Core\ModePertemuan;
use App\Core\RuanganId;
use App\Core\TopikPerkuliahan;
use App\Infrastructure\Repository\SqlServer\KelasRepository;
use App\Infrastructure\Repository\SqlServer\PertemuanRepository;
use DateTime;
use InvalidArgumentException;

class BuatPertemuanCommand
{
    public function __construct(
        private KelasRepository $kelasRepository,
        private PertemuanRepository $pertemuanRepository
    ) { }

    public function execute(BuatPertemuanRequest $request) : void
    {
        $kelasId = new KelasId($request->kelasId);
        $kelas = $this->kelasRepository->byId($kelasId);
        
        if (!$kelas) {
            throw new InvalidArgumentException('kelas_tidak_ditemukan');
        }
        
        $ruanganId = null;
        if ($request->ruanganId) {
            $ruanganId = new RuanganId($request->ruanganId);
        }

        $jadwal = new JadwalPertemuan(
            new DateTime($request->tanggal),
            new DateTime($request->jamMulai),
            new DateTime($request->jamSelesai)
        );

        $topik = new TopikPerkuliahan($request->topik, $request->topikEn);

        $mode = new ModePertemuan($request->mode);

        $pertemuan = $kelas->buatPertemuan(
            urutan: intval($request->pertemuanKe),
            ruanganId: $ruanganId,
            jadwal: $jadwal,
            topik: $topik,
            mode: $mode
        );

        $this->pertemuanRepository->save($pertemuan);
    }
}