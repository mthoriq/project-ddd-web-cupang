<?php

namespace App\Application\Query\Pertemuan;

class PertemuanDto
{
    public function __construct(
        public string $pertemuan_id,
        public string $kelas_id,
        public int $urutan,
        public string $kode_mk,
        public string $nama_mk,
        public string $nama_kelas,
        public string $tanggal,
        public string $jam_mulai,
        public string $jam_selesai,
        public ?string $nama_pengajar,
        public ?string $topik_kuliah,
    )
    { }
    
    
}