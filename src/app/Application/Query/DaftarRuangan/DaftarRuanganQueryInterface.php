<?php

namespace App\Application\Query\DaftarRuangan;

interface DaftarRuanganQueryInterface
{
    public function execute() : array;
}