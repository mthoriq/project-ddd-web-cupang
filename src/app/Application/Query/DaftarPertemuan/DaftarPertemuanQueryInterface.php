<?php

namespace App\Application\Query\DaftarPertemuan;

interface DaftarPertemuanQueryInterface
{
    public function execute(string $kelasId) : array;
}