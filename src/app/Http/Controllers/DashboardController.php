<?php

namespace App\Http\Controllers;

use App\Application\Query\DaftarKelasDosen\DaftarKelasDosenQueryInterface;
use App\Application\Query\Dosen\DosenQueryInterface;
use App\Application\Query\SemesterAktif\SemesterAktifQueryInterface;
use App\Http\Controllers\Controller;
use Dptsi\Sso\Facade\Sso;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function __construct(
        private DosenQueryInterface $dosenQuery,
        private DaftarKelasDosenQueryInterface $daftarKelasDosenQuery,
        private SemesterAktifQueryInterface $semesterAktifQuery
    ) { }

    public function index(Request $request)
    {
        $user = Sso::user();

        $dosen = $this->dosenQuery->execute($user->getId());

        $semester_aktif = $this->semesterAktifQuery->execute();

        $list_kelas = $this->daftarKelasDosenQuery->execute($dosen->dosen_id, $semester_aktif->semester_id);

        return view('dashboard.dosen', [
            'semester' => $semester_aktif,
            'dosen' => $dosen,
            'list_kelas' => $list_kelas
        ]);
    }
}