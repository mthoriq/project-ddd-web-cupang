<?php

namespace App\Http\Controllers;

use App\Application\Query\Kelas\KelasQueryInterface;
use App\Application\Query\DaftarPertemuan\DaftarPertemuanQueryInterface;
use App\Http\Controllers\Controller;
use Dptsi\Sso\Facade\Sso;
use Illuminate\Http\Request;

class KelasController extends Controller
{

    public function __construct(
        private KelasQueryInterface $kelasQuery,
        private DaftarPertemuanQueryInterface $pertemuanQuery
    ) { }

    public function index(Request $request)
    {
        $user = Sso::user();

       
    }

    public function get($id)
    {
        $kelas = $this->kelasQuery->execute(kelasId: $id);

        $list_pertemuan = $this->pertemuanQuery->execute(kelasId: $id);

        return view('kelas.dosen', [
            'kelas' => $kelas,
            'list_pertemuan' => $list_pertemuan
        ]);        
    }
}