<?php

namespace App\Http\Controllers;

use App\Application\Command\BuatPertemuan\BuatPertemuanCommand;
use App\Application\Command\BuatPertemuan\BuatPertemuanRequest;
use App\Application\Query\DaftarHadirMahasiswa\DaftarHadirMahasiswaQueryInterface;
use App\Application\Query\DaftarRuangan\DaftarRuanganQueryInterface;
use App\Application\Query\Dosen\DosenQueryInterface;
use App\Application\Query\Pertemuan\PertemuanQueryInterface;
use App\Core\Repository\KelasRepositoryInterface;
use App\Core\Repository\PertemuanRepositoryInterface;
use App\Http\Controllers\Controller;
use Dptsi\Sso\Facade\Sso;
use Illuminate\Http\Request;

class PertemuanController extends Controller
{

    public function __construct(
        private PertemuanQueryInterface $pertemuanQuery,
        private DaftarHadirMahasiswaQueryInterface $daftarHadirMahasiswaQuery,
        private DosenQueryInterface $dosenQuery,
        private DaftarRuanganQueryInterface $daftarRuanganQuery,
        private KelasRepositoryInterface $kelasRepository,
        private PertemuanRepositoryInterface $pertemuanRepository
    ) { }

    public function index(Request $request)
    {
        $user = Sso::user();
    }

    public function get($id)
    {
        $pertemuan = $this->pertemuanQuery->execute(pertemuanId: $id);

        if (!$pertemuan) {
            return "Pertemuan tidak ditemukan";
        }

        $daftarHadirMahasiswa = $this->daftarHadirMahasiswaQuery->execute(pertemuanId: $id);

        return view('pertemuan.dosen', [
            'pertemuan' => $pertemuan,
            'daftar_hadir_mahasiswa' => $daftarHadirMahasiswa
        ]);        
    }

    public function tambah($id_kelas)
    {
        $user = Sso::user();
        if($user->getActiveRole()->getName() !== 'Dosen') {
            abort(403);
        }

        $list_ruangan = $this->daftarRuanganQuery->execute();

        return view('pertemuan.tambah', [
            'list_ruangan' => $list_ruangan,
            'id_kelas' => $id_kelas
        ]);
    }

    public function tambahAction(Request $request, $id_kelas)
    {
        $user = Sso::user();
        if($user->getActiveRole()->getName() !== 'Dosen') {
            abort(403);
        }

        $dosenId = $this->dosenQuery->execute($user->getId())->dosen_id;
        $pertemuanKe = $request->input('pertemuan-ke');
        $ruanganId = $request->input('ruangan');
        $tanggal = $request->input('tanggal');
        $jamMulai = $request->input('jam-mulai');
        $jamSelesai = $request->input('jam-selesai');
        $topik = $request->input('topik');
        $topikEn = $request->input('topik-en');
        $mode = $request->input('mode-perkuliahan');

        $tambahRequest = new BuatPertemuanRequest(
            $id_kelas,
            $dosenId,
            $pertemuanKe,
            $ruanganId,
            $tanggal,
            $jamMulai,
            $jamSelesai,
            $topik,
            $topikEn,
            $mode
        );

        $service = new BuatPertemuanCommand(
            kelasRepository: $this->kelasRepository,
            pertemuanRepository: $this->pertemuanRepository
        );

        $service->execute($tambahRequest);

        return response()->redirectTo(route('kelas', ['id' => $id_kelas]))
            ->with('success', 'berhasil_membuat_tatap_muka');
    }
}