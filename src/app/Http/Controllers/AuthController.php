<?php

namespace App\Http\Controllers;

use Dptsi\Sso\Facade\Sso;
use Dptsi\Sso\Requests\OidcLoginRequest;
use Dptsi\Sso\Requests\OidcLogoutRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Its\Sso\OpenIDConnectClientException;

class AuthController extends Controller
{
    public function auth()
    {
        try {
            $request = new OidcLoginRequest(
                config('openid.provider'),
                config('openid.client_id'),
                config('openid.client_secret'),
                config('openid.redirect_uri'),
                config('openid.scope'),
                config('openid.allowed_roles')
            );

            Sso::login($request);

            return redirect()->to('/');
        } catch (OpenIDConnectClientException $e) {
            Session::remove('auth');
            Session::save();

            Log::error($e->getMessage());
        }
    }

    public function logout()
    {
        try {
            Auth::logout();

            $request = new OidcLogoutRequest(
                config('openid.provider'),
                config('openid.client_id'),
                config('openid.client_secret'),
                config('openid.post_logout_redirect_uri')
            );

            Sso::logout($request);
        } catch (OpenIDConnectClientException $e) {
            Log::error($e->getMessage());
        }
    }
}
