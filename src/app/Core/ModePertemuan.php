<?php

namespace App\Core;

use InvalidArgumentException;

class ModePertemuan
{
    const MODE_PERTEMUAN_ONLINE = 'D';
    const MODE_PERTEMUAN_OFFLINE = 'L';
    const MODE_PERTEMUAN_HYBRID = 'H';
    
    private string $mode;

    public function __construct(string $mode)
    {
        if ($mode != self::MODE_PERTEMUAN_ONLINE & 
            $mode != self::MODE_PERTEMUAN_OFFLINE &
            $mode != self::MODE_PERTEMUAN_HYBRID) {
            throw new InvalidArgumentException('mode_tatap_muka_tidak_sesuai');
        }

        $this->mode = $mode;
    }

    public function getMode() : string
    {
        return $this->mode;
    }

    public function equals(ModePertemuan $modeTatapMuka) : string
    {
        return $this->mode === $modeTatapMuka->getMode();
    }

}