<?php

namespace App\Core\Repository;

use App\Core\Pertemuan;

interface PertemuanRepositoryInterface
{
    public function save(Pertemuan $pertemuan): void;
}
