<?php

namespace App\Core\Repository;

use App\Core\Kelas;
use App\Core\KelasId;

interface KelasRepositoryInterface
{
    public function byId(KelasId $id): ?Kelas;
}