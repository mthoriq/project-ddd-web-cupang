<?php

namespace App\Core;

use App\Core\PertemuanId;
use App\Core\Exception\KelasException;

class Kelas
{
    private KelasId $id;
    private int $rencanaPertemuan;
    private bool $permanen;

    public function __construct(KelasId $id, int $rencanaPertemuan, bool $permanen)
    {
        $this->id = $id;
        $this->rencanaPertemuan = $rencanaPertemuan;
        $this->permanen = $permanen;
    }

    public function getId() : KelasId
    {
        return $this->id;
    }

    public function getRencanaPertemuan() : int
    {
        return $this->rencanaPertemuan;
    }

    public function isPermanen() : bool
    {
        return $this->permanen;
    }

    public function buatPertemuan(
        int $urutan,
        ?RuanganId $ruanganId,
        JadwalPertemuan $jadwal,
        TopikPerkuliahan $topik,
        ModePertemuan $mode) : Pertemuan
    {
        if ($this->rencanaPertemuan <= 0) {
            throw new KelasException('tidak_dapat_buat_pertemuan_baru_karena_belum_ada_rencana_pertemuan');
        }

        if ($this->isPermanen()) {
            throw new KelasException('tidak_dapat_membuat_pertemuan_baru_karena_nilai_sudah_permanen');
        }

        return new Pertemuan(
            new PertemuanId(),
            $this->id,
            $urutan,
            $ruanganId,
            $jadwal,
            $topik,
            $mode,
            null
        );
    }

}
