<?php

namespace App\Core;

use Ramsey\Uuid\Uuid;

class RuanganId
{
    private $id;

    public function __construct(string $id)
    {
        if (Uuid::isValid($id)) {
            $this->id = $id;
        } else {
            throw new \InvalidArgumentException("Invalid RuanganId format.");
        }
    }

    public function id() : string
    {
        return $this->id;
    }

    public function equals(RuanganId $ruanganId)
    {
        return $this->id === $ruanganId->id;
    }
}