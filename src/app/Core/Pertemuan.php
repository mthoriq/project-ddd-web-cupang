<?php

namespace App\Core;

use InvalidArgumentException;

class Pertemuan
{
    private PertemuanId $id;
    private KelasId $kelasId;
    private int $pertemuanKe;
    private ?RuanganId $ruanganId;
    private JadwalPertemuan $jadwal;
    private ?TopikPerkuliahan $topik;
    private ModePertemuan $mode;

    public function __construct(
        PertemuanId $id,
        KelasId $kelasId,
        int $pertemuanKe,
        ?RuanganId $ruanganId = NULL,
        JadwalPertemuan $jadwal,
        ?TopikPerkuliahan $topik = NULL,
        ModePertemuan $mode
    )
    {
        if ($pertemuanKe <= 0) {
            throw new InvalidArgumentException('urutan_harus_lebih_besar_dari_0');
        }

        if ($mode->getMode() == ModePertemuan::MODE_PERTEMUAN_OFFLINE && $ruanganId == null) {
            throw new InvalidArgumentException('mode_tatap_muka_offline_harus_memiliki_ruangan');
        }

        if ($mode->getMode() == ModePertemuan::MODE_PERTEMUAN_HYBRID && $ruanganId == null) {
            throw new InvalidArgumentException('mode_tatap_muka_hybrid_harus_memiliki_ruangan');
        }

        $this->id = $id;
        $this->kelasId = $kelasId;
        $this->pertemuanKe = $pertemuanKe;
        $this->ruanganId = $ruanganId;
        $this->jadwal = $jadwal;
        $this->topik = $topik;
        $this->mode = $mode;
    }

    public function getPertemuanId() : PertemuanId
    {
        return $this->id;
    }

    public function getKelasId() : KelasId
    {
        return $this->kelasId;
    }

    public function getPertemuanKe() : int
    {
        return $this->pertemuanKe;
    }

    public function getRuanganId() : ?RuanganId
    {
        return $this->ruanganId;
    }

    public function getJadwal() : JadwalPertemuan
    {
        return $this->jadwal;
    }

    public function getTopik() : ?TopikPerkuliahan
    {
        return $this->topik;
    }

    public function getMode() : ModePertemuan
    {
        return $this->mode;
    }



}
