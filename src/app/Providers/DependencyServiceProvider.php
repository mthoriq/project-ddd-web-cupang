<?php

namespace App\Providers;

use App\Application\Query\DaftarKelasDosen\DaftarKelasDosenQueryInterface;
use App\Application\Query\DaftarHadirMahasiswa\DaftarHadirMahasiswaQueryInterface;
use App\Application\Query\Dosen\DosenQueryInterface;
use App\Application\Query\Kelas\KelasQueryInterface;
use App\Application\Query\DaftarPertemuan\DaftarPertemuanQueryInterface;
use App\Application\Query\DaftarRuangan\DaftarRuanganQueryInterface;
use App\Application\Query\Pertemuan\PertemuanQueryInterface;
use App\Application\Query\SemesterAktif\SemesterAktifQueryInterface;
use App\Core\Repository\KelasRepositoryInterface;
use App\Core\Repository\PertemuanRepositoryInterface;
use App\Infrastructure\Query\SqlServer\DaftarKelasDosenQuery;
use App\Infrastructure\Query\SqlServer\DaftarHadirMahasiswaQuery;
use App\Infrastructure\Query\SqlServer\DosenQuery;
use App\Infrastructure\Query\SqlServer\KelasQuery;
use App\Infrastructure\Query\SqlServer\DaftarPertemuanQuery;
use App\Infrastructure\Query\SqlServer\DaftarRuanganQuery;
use App\Infrastructure\Query\SqlServer\PertemuanQuery;
use App\Infrastructure\Query\SqlServer\SemesterAktifQuery;
use App\Infrastructure\Repository\SqlServer\KelasRepository;
use App\Infrastructure\Repository\SqlServer\PertemuanRepository;
use Illuminate\Support\ServiceProvider;

class DependencyServiceProvider extends ServiceProvider
{
    public function register()
    {
        // Query
        $this->app->bind(DaftarKelasDosenQueryInterface::class, DaftarKelasDosenQuery::class);
        $this->app->bind(DosenQueryInterface::class, DosenQuery::class);
        $this->app->bind(SemesterAktifQueryInterface::class, SemesterAktifQuery::class);
        $this->app->bind(KelasQueryInterface::class, KelasQuery::class);
        $this->app->bind(DaftarPertemuanQueryInterface::class, DaftarPertemuanQuery::class);
        $this->app->bind(DaftarHadirMahasiswaQueryInterface::class, DaftarHadirMahasiswaQuery:: class);
        $this->app->bind(PertemuanQueryInterface::class, PertemuanQuery::class);
        $this->app->bind(DaftarRuanganQueryInterface::class, DaftarRuanganQuery::class);

        // Repository
        $this->app->bind(KelasRepositoryInterface::class, KelasRepository::class);
        $this->app->bind(PertemuanRepositoryInterface::class, PertemuanRepository::class);
    }
}