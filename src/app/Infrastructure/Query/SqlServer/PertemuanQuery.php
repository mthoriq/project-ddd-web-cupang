<?php

namespace App\Infrastructure\Query\SqlServer;

use App\Application\Query\Pertemuan\PertemuanDto;
use App\Application\Query\Pertemuan\PertemuanQueryInterface;
use Carbon\CarbonImmutable;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\DB;

class PertemuanQuery implements PertemuanQueryInterface
{

    public function execute(string $pertemuanId): ?PertemuanDto
    { 
        $sql = "SELECT t.id_pertemuan_kuliah, r.kode as ruangan, t.pertemuan_ke, t.tgl_kuliah,
                t.jam_mulai, t.jam_selesai, t.topik_kuliah, t.topik_kuliah_en, d.nama as nama_dosen,
                mk.kode_mk, mk.nama AS nama_mk, k.id_kelas, k.nama AS nama_kelas, t.bentuk_tm,
                h.bentuk_hadir AS bentuk_hadir_dosen, h.is_lupa_presensi, t.berlaku_sampai,
                h.jam_mulai AS jam_mulai_mengajar, h.jam_selesai AS jam_selesai_mengajar, h.id_dosen
                FROM pertemuan_kuliah t
                LEFT JOIN kehadiran_dosen h ON h.id_pertemuan_kuliah = t.id_pertemuan_kuliah
                LEFT JOIN dosen d ON d.id_dosen = h.id_dosen
                LEFT JOIN ruangan r ON r.id_ruangan = t.id_ruangan
                INNER JOIN kelas k ON k.id_kelas = t.id_kelas
                INNER JOIN mata_kuliah mk on mk.id_mk = k.id_mk
                WHERE t.id_pertemuan_kuliah = :pertemuan_id
                ORDER BY t.pertemuan_ke";

        $pertemuan = DB::selectOne($sql,[
            'pertemuan_id' => $pertemuanId
        ]);

        if (empty($pertemuan)) {
            return null;
        }

        return new PertemuanDto(
            pertemuan_id: $pertemuan->id_pertemuan_kuliah,
            kode_mk: $pertemuan->kode_mk,
            nama_mk: $pertemuan->nama_mk,
            kelas_id: $pertemuan->id_kelas,
            nama_kelas: $pertemuan->nama_kelas,
            urutan: $pertemuan->pertemuan_ke,
            tanggal: CarbonImmutable::parse($pertemuan->tgl_kuliah)->locale('id')->setTimezone('Asia/Jakarta')->isoFormat('dddd, D MMMM YYYY'),
            jam_mulai: date_format(new DateTime($pertemuan->jam_mulai, new DateTimeZone("Asia/Jakarta")), "H:i"),
            jam_selesai: date_format(new DateTime($pertemuan->jam_selesai, new DateTimeZone("Asia/Jakarta")), "H:i"),
            nama_pengajar: $pertemuan->nama_dosen,
            topik_kuliah: $pertemuan->topik_kuliah,
        );
    }

}