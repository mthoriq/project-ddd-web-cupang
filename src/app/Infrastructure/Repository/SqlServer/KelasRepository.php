<?php

namespace App\Infrastructure\Repository\SqlServer;

use App\Core\Kelas;
use App\Core\KelasId;
use App\Core\Repository\KelasRepositoryInterface;
use Illuminate\Support\Facades\DB;

class KelasRepository implements KelasRepositoryInterface
{

    public function byId(KelasId $id): ?Kelas
    {
        $sql = "SELECT k.id_kelas, k.rencana_tm, k.is_nilai_final
                FROM kelas k
                WHERE k.id_kelas = :id_kelas AND k.deleted_at IS NULL";

        $result = DB::select($sql, [
            'id_kelas' => $id->id()
        ]);

        if ($result) {
            return new Kelas(
                id: new KelasId($result[0]->id_kelas),
                rencanaPertemuan: $result[0]->rencana_tm,
                permanen: $result[0]->is_nilai_final
            );
        }

        return null;
    }

}
