<?php

namespace App\Infrastructure\Repository\SqlServer;

use App\Core\Pertemuan;
use App\Core\Repository\PertemuanRepositoryInterface;
use DateTime;
use Illuminate\Support\Facades\DB;

class PertemuanRepository implements PertemuanRepositoryInterface
{

    public function save(Pertemuan $pertemuan): void
    {
        $data = [
            'id_pertemuan_kuliah' => $pertemuan->getPertemuanId()->id(),
            'id_kelas' => $pertemuan->getKelasId()->id(),
            'id_ruangan' => $pertemuan->getRuanganId()?->id(), 
            'pertemuan_ke' => $pertemuan->getPertemuanKe(),
            'tgl_kuliah' => $pertemuan->getJadwal()->getTanggal(),
            'jam_mulai' => $pertemuan->getJadwal()->getJamMulai(),
            'jam_selesai' => $pertemuan->getJadwal()->getJamSelesai(),
            'topik_kuliah' => $pertemuan->getTopik()?->getDeskripsi('id'),
            'topik_kuliah_en' => $pertemuan->getTopik()?->getDeskripsi('en'),
            'bentuk_tm' => $pertemuan->getMode()->getMode(),
            'created_at' => (new DateTime())->format('Y-m-d H:i:s'),
            'updated_at' => (new DateTime())->format('Y-m-d H:i:s'),
            'deleted_at' => null
        ];

        $sql = "INSERT INTO pertemuan_kuliah(id_pertemuan_kuliah, id_kelas, id_ruangan,
                    pertemuan_ke, tgl_kuliah, jam_mulai, jam_selesai,
                    topik_kuliah, topik_kuliah_en, bentuk_tm, 
                    created_at, updated_at, deleted_at)
                    VALUES (:id_pertemuan_kuliah, :id_kelas, :id_ruangan,
                    :pertemuan_ke, :tgl_kuliah, :jam_mulai, :jam_selesai,
                    :topik_kuliah, :topik_kuliah_en, :bentuk_tm, 
                    :created_at, :updated_at, :deleted_at)";

        DB::insert($sql, $data);
    }

}
