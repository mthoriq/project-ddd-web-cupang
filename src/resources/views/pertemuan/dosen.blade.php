@extends('layouts.app')

@section('title', 'Daftar Presensi Pertemuan ke-'.$pertemuan->urutan)

@section('content')
<div class="row mb-3">
    <div class="col-md-12">
        <a href="/kelas/{{ $pertemuan->kelas_id }}" class="btn btn-outline-secondary"><i class="bi bi-chevron-left"></i> Kembali</a>
    </div>
</div>

<div class="container">
    <div class="row bg-light p-3 mb-3 align-items-center">
        <div class="col-md-9">
            <h5 class="mb-1">
                <span class="badge bg-primary">{{ $pertemuan->kode_mk }}</span>
                <p class="no-margin mt-2">
                    {{ $pertemuan->nama_mk }} ({{ $pertemuan->nama_kelas }})
                </p>
            </h5>
            <p class="mb-2">
                <i class="bi bi-calendar mr-1"></i> {{ $pertemuan->tanggal }}
                <i class="bi bi-clock mr-1 ml-10"></i> {{ $pertemuan->jam_mulai }} - {{ $pertemuan->jam_selesai }}
            </p>
            <p class="mb-2">
                Topik Perkuliahan:<br><strong>{{ $pertemuan->topik_kuliah }}</strong>
            </p>
            @if ($pertemuan->nama_pengajar)
            <p class="mb-0">
                Nama pengajar:<br><strong>{{ $pertemuan->nama_pengajar }}</strong>
            </p>
            @endif
        </div>
    </div>

    @php
        $jml_hadir = 0;
        $jml_izin = 0;
        $jml_sakit = 0;
        $jml_alpa = 0;
        $total = 0;

        foreach($daftar_hadir_mahasiswa as $mhs) {
            if ($mhs->jenis_kehadiran === NULL || $mhs->jenis_kehadiran === 'A') {
                $jml_alpa++; 
            } elseif ($mhs->jenis_kehadiran === 'I') {
                $jml_izin++;
            } elseif ($mhs->jenis_kehadiran === 'S') {
                $jml_sakit++;
            } elseif ($mhs->jenis_kehadiran === 'H') {
                $jml_hadir++;
            }
            $total++;
        }
    @endphp

    <div class="container bg-light p-3 mb-3">
        <div class="d-flex flex-column flex-md-row justify-content-md-between">
            <ul class="ps-0 mb-3 mb-md-0 row" style="list-style-type: none;">
                <li class="col-3 text-center px-4">
                    <strong class="text-success" style="font-size: 0.8rem">HADIR</strong>
                    <h4 class="m-0 fw-bold count-kehadiran" data-jenis-hadir="H">{{ $jml_hadir }}</h4>
                </li>
                <li class="col-3 border-start text-center px-4">
                    <strong class="text-primary" style="font-size: 0.8rem">IZIN</strong>
                    <h4 class="m-0 fw-bold count-kehadiran" data-jenis-hadir="I">{{ $jml_izin }}</h4>
                </li>
                <li class="col-3 border-start text-center px-4">
                    <strong class="text-warning" style="font-size: 0.8rem">SAKIT</strong>
                    <h4 class="m-0 fw-bold count-kehadiran" data-jenis-hadir="S">{{ $jml_sakit }}</h4>
                </li>
                <li class="col-3 border-start text-center px-4">
                    <strong class="text-danger" style="font-size: 0.8rem">ALPA</strong>
                    <h4 class="m-0 fw-bold count-kehadiran" data-jenis-hadir="A">{{ $jml_alpa }}</h4>
                </li>
            </ul>
            <div class="text-center text-md-end">
                <strong style="font-size: 0.8rem">
                    TOTAL MAHASISWA
                </strong>
                <h4 class="fw-bold count-kehadiran" data-jenis-hadir="T">
                    {{ $total }}
                </h4>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="table-responsive d-none d-lg-block">
            <table class="table table-vcenter js-hadir-mhs-dsn">
                <thead class="text-center bg-body-light">
                    <tr>
                        <th class="text-start">NRP / Nama</th>
                        <th style="width: 25%" class="text-end">Kehadiran</th>
                    </tr>
                </thead>
                <tbody class="font-w600 text-center">
                    @foreach ($daftar_hadir_mahasiswa as $mhs)
                    <tr>
                        <td class="text-start py-3">
                            <p class="font-w300 font-size-xs m-0">{{ $mhs->nrp }}</p>
                            <span class="text-uppercase">{{ $mhs->nama }}</span>
                        </td>
                        <td class="kehadiran-mahasiswa align-middle text-end">
                            <div class="form-group">

                                @if ($mhs->jenis_kehadiran === NULL || $mhs->jenis_kehadiran === 'A')
                                    ALPA 
                                @elseif ($mhs->jenis_kehadiran === 'I')
                                    IZIN
                                @elseif ($mhs->jenis_kehadiran === 'S')
                                    SAKIT
                                @elseif ($mhs->jenis_kehadiran === 'H')
                                    HADIR
                                @endif

                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection